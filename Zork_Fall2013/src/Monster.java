
/* A monster is a participant that the player may encounter while traversing
 * the dungeon. If a monster is encountered then the player must defeat
 * the monster before continuing through the dungeon */
public class Monster extends Participant
{
	/* The monsters in the game begin with 20 health points and 4 hit points.
	 * they also have the name "monster" */
	public Monster ( )
	{
		this.healthPoints = 20;
		//this.hitPoints = 4;
		weaponType = new MonsterAttacksWithBareHands ( );
		this.name = "monster";
	}
	
	/* When a monster is killed then rather than create a new monster for the
	 * next encounter the current monster is "recycled". This saves the 
	 * computer time for what would be new memory allocation. */
	public void restorePoints ( )
	{
		this.healthPoints = 20;
	}
}
