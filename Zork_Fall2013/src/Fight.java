import java.util.Random;

/* A fight takes place between two participants; a player and a monster. The 
 * outcome of the fight determines if the player continues or if the game has
 * been lost */
public class Fight
{
	private Participant player;
	private Participant monster;
	
	public Fight ( )
	{
		
	}
	
	public Fight (Participant player, Participant monster)
	{
		setPlayer (player);
		setMonster (monster);
	}
	
	public void setPlayer (Participant player)
	{
		this.player = player;
	}
	
	public void setMonster (Participant monster)
	{
		this.monster = monster;
	}
	
	public Participant getPlayer ( )
	{
		return this.player;
	}
	
	public Participant getMonster ( )
	{
		return this.monster;
	}
	
	
	/* The player and the monster take turns attacking each other. The monster 
	 * has a 20% chance to hit the player. The player has a 90% chance to hit
	 * the monster. When one of the participants is dead the then appropriate
	 * notification is sent to the driver program. If the monster has been
	 * defeated then the game continues. If the player has been defeated then
	 * the game ends.*/
	public int fightMonster ( )
	{
		Random ran = new Random ( );
		String message;
		
		while (true)
		{
			/* The player attacks the monster */
			if (ran.nextInt (10000) > 9000)
			{
				message = "\tThe player missed the monster" ;
			}
			else
			{
				message = this.player.attack(player, monster);
			}
			
			if (monster.getHealthPoints ( ) <= 0)
			{
				System.out.println (message);
				message = "\tThe monster is Dead";
				System.out.println (message);
				return 1;
			}
			System.out.println (message);
			
			/* The monster attacks the player */
			if (ran.nextInt (10000) < 2000)
			{
				message = "\tThe monster missed the player";
			}
			else
			{
				message = monster.attack(monster, player);
			}
			if (player.getHealthPoints ( ) <= 0)
			{
				System.out.println (message);
				return 2;
			}
			System.out.println (message);
		}
	}
	

}
