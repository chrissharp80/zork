import java.util.Scanner;

/* Zork is a classic text based game in which a player is moving through a 
 * dungeon. This simple example serves as an example of some object oriented
 * programming principles */
public class Zork
{
	private static Scanner kb; 
	
	public static void main (String[] args)
	{
		kb = new Scanner(System.in);

		Participant monster;		
		Participant player;		
		Dungeon dungeon;
		Fight fight;
		Cell cell;			
		String direction;		
		String weaponType;		
		String hasMonster;	
		int fightOutcome;		
		int playerPosition;		
		int totalColumns;			

		dungeon = new Dungeon ( );
		monster = new Monster ( );
		player = new Player ( );
		fight = new Fight (player, monster);
		dungeon.numCols ( );
		dungeon.buildDungeon ( );
		playerPosition = ((Player) player).getPosition ( );
		totalColumns = dungeon.getCols ( );
		
		while (true)
		{
			showDungeon (player, dungeon);
			
			/* Collects and stores the users choice to go east or west */
			direction = (kb.nextLine ( )).toLowerCase ( );
			
			/* Gets and modifies the cell the player is in before moving.
			 * this prepares the cell for the movement the player is about
			 * to execute */
			cell = dungeon.getCell(playerPosition);
			cell.modifyCell(0);
			
			/* Checks to see if the player is about to exit the dungeon. If so
			 * then the program knows the player has defeated the dungeon and
			 * the program exits */
			if (playerPosition == totalColumns - 1 && direction.equals("go east"))
			{
				System.out.println ("\n\nThe game is over!!!\n\nYou have"  
									+ " beaten the dungeon and all of its"
									+ " monsters! congratulations!");
				System.exit(0);
			}
			
			/* Sends the answer and the current position, as well as the 
			 * total number of columns, to the method and if the player's
			 * chosen move is possible the players position is updated.
			 * Note: This does not update the dungeon but does provide
			 * the information needed to move the player and update the
			 * dungeon to reflect that move */
			((Player) player).move(direction, playerPosition, totalColumns);
			
			/* Stores the players new position */
			playerPosition = ((Player) player).getPosition ( );
			
			/* The new cell is updated to include the player */
			cell = dungeon.getCell(playerPosition);
			cell.modifyCell(1);
			
			/* The new cell is checked for a weapon and if one is found the
			 * type of the weapon is identified and the additional hit points
			 * are added to the players hit points attribute. */
			weaponType = cell.checkCellWeapon ( );
			if (weaponType == "W")
			{
				System.out.println ("\nYou found a sword!"
									+" Hit points increased to 8");
				player.setWeaponUsed (new AttackWithSword ( ));
				//((Player) p).modifyHitPoints(3);
			}
			else if(weaponType == "T")
			{
				System.out.println ("\nYou found a stick!" 
									+" Hit points increased to 6\n");
				player.setWeaponUsed (new AttackWithStick ( ));
				//((Player) p).modifyHitPoints(1);
			}
			
			/* The new cell is checked for a monster and if one is found then
			 * a fight between the monster and the player ensues. */
			hasMonster = cell.checkCellMonster ( );
			if (hasMonster == "M")
			{
				System.out.println ("There is a monster here!"
									+" The fight begins");
				fightOutcome = fight.fightMonster( );
				if (fightOutcome == 1)
				{
					cell.redrawCell(1);
					((Monster) monster).restorePoints();
				}
				else if (fightOutcome == 2)
				{
					System.out.println ("You were killed. Game Over");
					System.exit (0);
				}
			}

		}
					
	}
	
	/* This method serves to make the main method more readable by displaying
	 * the layout of the board here, rather than having so many print 
	 * statements in main */
	public static void showDungeon (Participant p, Dungeon d)
	{
		String gameboard;	// String representing the game at the current time
		String layout;		// String representing the dungeon
		String message;		// Message if player cannot go west
		
		layout = d.toString ( );
		message = p.getMessage ( );
		
		if (message.length ( ) > 0)
		{
			layout = d.toString ( );
			System.out.println(message);
		}
		
		gameboard = "\n\n" + layout;
		gameboard += "\nPlayer health points: " + p.getHealthPoints ( );
		gameboard += "\n\nWhat would you like to do next? Your choices are "
				  + "'go east' and 'go west'";
		
		System.out.println (gameboard);
	}

}
