
/* A player is the main character in the game. Players are participants that
 * the user controls. The object of the game is to get the player from the
 * beginning of the dungeon to the exit */
public class Player extends Participant
{
	/* A player starts out with an initial position of 0 (the farthest cell to
	 * the west of the dungeon), 100 health points, 5 hit points, and the name
	 * "player". The player's message, by default, is nothing */
	public Player ( )
	{
		this.currentPosition = 0;
		this.message = "";
		this.healthPoints = 100;
		weaponType = new PlayerAttacksWithBareHands ( );
		//this.hitPoints = 5;
		this.name = "player";
	}
	
	/* Getter for the current position of the player */
	public int getPosition ( )
	{
		return this.currentPosition;
	}
	
	/* When a weapon is found the player's hit points are increased. A sword
	 * increases the hit points from 5 to 8 and a stick increases them from 
	 * 5 to 6. */
	public void modifyHitPoints (int points)
	{
		//this.hitPoints += points;
	}
	
	/* The player is moved one position each time the method is called. If
	 * the player is already in the western most cell then futher western
	 * movement is not possible. */
	public void move (String direction, int position, int cols)
	{
		
		if (direction.equals ("go west"))
		{
			if (position > 0)
			{
				this.currentPosition -= 1;
			}
			else
			{
				this.message = "Sorry, but I can't go in that direction";
			}
		}
		else if (direction.equals ("go east"))
		{
			if (position < cols - 1)
			{
				this.currentPosition += 1;
				this.message = "";
			}

		}
		else
		{
			this.message = "Please enter a valid direction";
		}
	}
}
