import java.util.Random;

/* The dungeon is the playing field of the game. It is guaranteed to have a 
 * player, an east, a west, and an exit. There can also be monsters and 
 * a single weapon in the dungeon.*/
public class Dungeon
{
	private Random ran = new Random ( ); // A random number generator
	private int cols; 					 // Number of columns in current game 
	private Cell[] dg;					 // A dungeon holds an array of cells
	
	/* The number of columns is determined by random generation. The 
	 * minimum number of columns is 5 and the maximum is 10. In this case
	 * the word columns is synonymous with cell */
	public void numCols ( )
	{
		int min = 5;
		int max = 10;
		cols = ran.nextInt((max - min) + 1) + min;;
	}
	
	/* Returns the number of columns used to build the dungeon */
	public int getCols ( )
	{
		return this.cols;
	}
	
	/* Returns a particular cell */
	public Cell getCell (int cols)
	{
		return dg[cols];
	}
	
	/* Uses random number generation to determine the presence of a monster
	 * and/or a weapon in each cell. The chance for a monster in any given
	 * cell is 50%, as is the chance for either a stick or a sword. However,
	 * after a weapon has been placed in a cell there can be no more weapons
	 * placed in the dungeon */
	public void buildDungeon ( )
	{
		dg = new Cell[cols];
		boolean isMonster;
		boolean hasWeapon;
		
		hasWeapon = false;
		
		for (int i = 0; i < dg.length; i++)
		{
			isMonster = chanceMonster ( );
			
			if (i == 0)
			{
				dg[i] = new Cell(true, false, false, false);
			}
			else if (isMonster == true && chanceWeapon ( ).equals ("ST") 
					 && hasWeapon == false) 
			{
				dg[i] = new Cell (false, true, true, false);
				hasWeapon = true;
			}
			else if (isMonster == true && chanceWeapon ( ).equals ("SW") 
					 && hasWeapon == false)
			{
				dg[i] = new Cell (false, true, false, true);
				hasWeapon = true;
			}
			else if (isMonster == true)
			{
				dg[i] = new Cell (false, true, false, false);
			}
			else if (isMonster == false && chanceWeapon ( ).equals ("ST") 
					 && hasWeapon == false)
			{
				dg[i] = new Cell (false, false, true, false);
				hasWeapon = true;
			}
			else if (isMonster == false && chanceWeapon ( ).equals ("SW") 
					 && hasWeapon == false)
			{
				dg[i] = new Cell (false, false, false, true);
				hasWeapon = true;
			}
			else
			{
				dg[i] = new Cell ( );
			}
		}
	}
	
	/* Determines the chance that a monster will be in a cell */
	public boolean chanceMonster ( )
	{
		Random ran = new Random ( );
		boolean isMonster = false;
		
		if (ran.nextInt (10000) < 5000)
		{
			isMonster = true;
		}
		return isMonster;
	}
	
	/* Determines the chance that a weapon will be in a cell */
	public String chanceWeapon ( )
	{
		Random ran = new Random ( );
		String weaponType = "";
		
		if (ran.nextInt (10000) < 5000)
		{
			weaponType = weaponType ( );
		}
		return weaponType;
	}
	
	/* Determines if the weapon will be a stick or a sword (50% chance) */
	public String weaponType ( )
	{
		String weaponType = "";
		Random ran = new Random ( );
		
		if (ran.nextInt (10000) < 5000)
		{
			weaponType = "ST";
		}
		else
		{
			weaponType = "SW";
		}
		return weaponType;
	}
	
	/* Displays the entire dungeon */
	public String toString ( )
	{
		String layout = "";
		
		for (int i = 0; i < cols; i++)
		{
			layout += dg[i];
			layout += "  ";
		}
		layout += "Exit";
		return layout;
	}
}
