//checking to see if this is on the dev branch
public class Cell
{
	private String[] cell = new String[6]; // Strings making up a cell
	
	/* The default cell is empty and looks as follows: |____| */
	public Cell ( )
	{
		for (int i = 0; i < 6; i++)
		{
			if (i == 0 || i == 5)
			{
				cell[i] = "|";
			}
			else
			{
				cell[i] = "_";
			}
		}
	}
	
	/* The parameterized cell can contain a player, a monster, and a weapon.
	 * The monster and weapon being in the cell is dependent on random number
	 * generation but the player's presence in cell (other than the starting
	 * cell) is caused by player movement. The cells are created depending
	 * on boolean values obtained by random number generation in the dungeon
	 * class */
	public Cell (boolean player, boolean monster, boolean stick, boolean sword)
	{
		cell[0] = "|";
		cell[5] = "|";
		
		if (player == true)
		{
			cell[1] = "P";
		}
		else 
		{
			cell[1] = "_";
		}
		
		if (monster == true)
		{
			cell[2] = "M";
		}
		else 
		{
			cell[2] = "_";
		}
		if (stick == true)
		{
			cell[3] = "S";
			cell[4] = "T";
		}
		else if (sword == true)
		{
			cell[3] = "S";
			cell[4] = "W";
		}
		else
		{
			cell[3] = "_";
			cell[4] = "_";
		}
	}
	
	/* A cell is modified if the player moves into or out of it */
	public String modifyCell (int i)
	{
		if (i == 1)
		{
			cell[1] = "P";
		}
		else if (i == 0)
		{
			cell[1] = "_";
		}
		
		return cell[1];
	}
	
	/* After a fight, the cell is redrawn to reflect that the monster has
	 * been destroyed */
	public void redrawCell (int fightOutcome)
	{
		if (fightOutcome == 1)
		{
			cell[2] = "_";
		}
		else if (fightOutcome == 2)
		{
			cell[1] = "_";
		}
	}
	
	/* Simply checks the cell the player entered for a monster */
	public String checkCellMonster ( )
	{	
		if (cell[2] == "M")
		{
			return cell[2];
		}
		return "";
	}
	
	/* Simply checks the cell the player entered for a weapon */
	public String checkCellWeapon ( )
	{	
		String type = "";
		
		if (cell[4] == "W" || cell[4] == "T")
		{
			type = cell[4];
			cell[3] = "_";
			cell[4] = "_";
		}
		return type;
	}
	
	/* Displays a cell object */
	public String toString ( )
	{
		String layout = "";
		
		for (int i = 0; i < 6; i++)
		{
			layout += cell[i];
		}
		return layout;
	}
}
