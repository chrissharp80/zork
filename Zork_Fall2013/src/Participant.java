
/* A participant is either a monster or a player, though other participants
 * could be added. The monsters and the players share many common methods
 * so it makes sense to include those common methods in a "parent" class
 * rather than to write them multiple times. */
public class Participant
{
	protected String message;
	protected String name;
	protected int healthPoints;
	protected int currentPosition;
	protected int hitPoints;
	public WeaponBehavior weaponType;
	
	/* Setter for health points */
	public void setHealthPoints (int points)
	{
		this.healthPoints = points;
	}
	
	/* Setter for hit points */
	public void setHitPoints (int points)
	{
		this.hitPoints = points;
	}
	
	/* Getter for the participant name (monster or player) */
	public String getName ( )
	{
		return this.name;
	}
	
	/* Getter for the current health points */
	public int getHealthPoints ( )
	{
		return this.healthPoints;
	}
	
	/* Getter for the hit points */
	public int getWeaponType ( )
	{
		return this.weaponType.addedHitPoints ( );
	}
	
	/* Getter for the messages generated during fights. Also used when the 
	 * player cannot move in the direction requested by the user */
	public String getMessage ( )
	{
		return this.message;
	}
	
	/* Modifies the health points of a participant during a fight. If the
	 * participant is struck then the appropriate number of health points 
	 * are deducted */
	public int modifyHealthPoints (int points)
	{
		this.healthPoints -= points;
		return this.healthPoints;
	}
	
	public int weaponUsed ( )
	{
		return weaponType.addedHitPoints ( );
	}
	
	public void setWeaponUsed (WeaponBehavior newWeaponType)
	{
		weaponType = newWeaponType;
	}
	
	/* If the attack was successful then the appropriate number of hit points
	 * is deducted from the participant and the proper messages are generated
	 * and passed back to be displayed to the user */
	public String attack (Participant p, Participant p2)
	{
		String message = "";
		int healthPoints;

		healthPoints = p2.modifyHealthPoints (p.weaponUsed ( )); 
		message = "\tThe " + p2.getName ( ) + " was hit by the " 
		          + p.getName ( ); 
		if (healthPoints > 0)
		{
			message += " : health points remaining = " + healthPoints;
		}
		else
		{
			message += " : points = " + 0;
		}
		return message;
	}
}
